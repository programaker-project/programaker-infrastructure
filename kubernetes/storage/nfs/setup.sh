#!/bin/sh

set -eu

## Install NFS client
helm fetch stable/nfs-client-provisioner -d ../../output/charts/

rm -Rf ../../output/templates/nfs/

helm template -f values.yaml --namespace=storage --name=nfs-client-provisioner ../../output/charts/nfs-client-provisioner-1.2.5.tgz \
     --output-dir ../../output/templates/

FILES=$(
    for f in ../../output/templates/nfs-client-provisioner/templates/*;do
        echo '-f' "$f"
    done)

kubectl -n storage apply $FILES
