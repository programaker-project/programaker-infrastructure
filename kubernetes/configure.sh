#!/bin/sh

set -eu

git submodule update --init --recursive .

## Create base
kubectl apply -f plaza-production/namespaces.yaml          \
                                                           \
              -f plaza-operation/namespaces.yaml           \
              -f plaza-operation/helm-sa.yaml              \
              -f plaza-operation/helm-clusterrole.yaml     \
              -f plaza-operation/helm-clusterrolebinding.yaml

## Install ingress
mkdir -p output/charts || true
mkdir -p output/templates/|| true
helm upgrade --install entrypoint ingress-nginx \
     --repo https://kubernetes.github.io/ingress-nginx \
     -f ingress/nginx-ingress-values.yaml  \
     --namespace plaza-production


# Install cert manager
cd cert-manager
sh setup.sh
cd ..
