#!/bin/sh

set -eu

kubectl create namespace cert-manager || true

kubectl -n cert-manager apply -f cert-manager.crds.v1.5.5.yaml
kubectl                 apply -f cert-manager.v1.5.5.yaml
kubectl -n cert-manager apply -f letsencrypt-stg.yaml
kubectl -n cert-manager apply -f letsencrypt.yaml
