#!/bin/sh

set -eu

kubectl -n cert-manager apply -f letsencrypt.yaml
kubectl -n cert-manager apply -f letsencrypt-stg.yaml
kubectl                 apply -f cert-manager.v1.5.5.yaml
kubectl -n cert-manager apply -f cert-manager.crds.v1.5.5.yaml

kubectl delete namespace cert-manager
