#!/bin/sh

set -u

if [ -z "${FORCE_CONTINUE:-}" ];then
    set -e
fi

cd "`dirname "$0"`"
pwd

get_deployment_pods() {
    namespace=${2:-plaza-production}

    # Lines taken from https://github.com/kubernetes/kubernetes/issues/72794#issuecomment-483502617
    selfLink=$(kubectl get deployment.apps "$1" -n "${namespace}" -o jsonpath={.metadata.selfLink})
    selector=$(kubectl get --raw "${selfLink}"/scale | jq -r .status.selector)

    # There's probably a less hackish way to do this...
    kubectl get pods -n "${namespace}" --selector "${selector}"|awk '{print $1;}'|tail -n+2
}

START_DATE=`date '+%Y-%m-%d_%H:%M:%S'`
mkdir "${START_DATE}"
cd "${START_DATE}"

echo "Core"
echo "Backend - 0" >&2
kubectl -n plaza-production exec plaza-backend-0 ash /app/scripts/backup.sh > plaza-backend-0.mnesia.backup 2>plaza-backend-0.mnesia.backup.sha1sum
cat plaza-backend-0.mnesia.backup.sha1sum | awk '{ print $1 "  plaza-backend-0.mnesia.backup"; }' > checksums.txt

echo "Backend - 1" >&2
kubectl -n plaza-production exec plaza-backend-1 ash /app/scripts/backup.sh > plaza-backend-1.mnesia.backup 2>plaza-backend-1.mnesia.backup.sha1sum
cat plaza-backend-1.mnesia.backup.sha1sum | awk '{ print $1 "  plaza-backend-1.mnesia.backup"; }' >> checksums.txt

echo "Backend - 2" >&2
kubectl -n plaza-production exec plaza-backend-2 ash /app/scripts/backup.sh > plaza-backend-2.mnesia.backup 2>plaza-backend-2.mnesia.backup.sha1sum
cat plaza-backend-2.mnesia.backup.sha1sum | awk '{ print $1 "  plaza-backend-2.mnesia.backup"; }' >> checksums.txt

# # Bridges are moving to a PostgreSQL DB
# echo "Telegram"
# kubectl -n plaza-production cp `get_deployment_pods plaza-telegram-bridge|head`:/db/telegram_db.sqlite3 telegram_db.sqlite3
# sha1sum telegram_db.sqlite3 >> checksums.txt

# echo "Toggl"
# kubectl -n plaza-production cp `get_deployment_pods plaza-toggl-bridge|head`:/db/toggl_db.sqlite3 toggl_db.sqlite3
# sha1sum toggl_db.sqlite3 >> checksums.txt

# echo "Twitter"
# kubectl -n plaza-production cp `get_deployment_pods plaza-twitter-bridge|head`:/db/twitter_db.sqlite3 twitter_db.sqlite3
# sha1sum twitter_db.sqlite3 >> checksums.txt

# echo "Gitlab"
# kubectl -n plaza-production cp `get_deployment_pods plaza-gitlab-bridge|head`:/db/gitlab_db.sqlite3 gitlab_db.sqlite3
# sha1sum gitlab_db.sqlite3 >> checksums.txt

# echo "Matrix"
# kubectl -n plaza-production cp `get_deployment_pods plaza-matrix-bridge|head`:/db/matrix_db.sqlite3 matrix_db.sqlite3
# sha1sum matrix_db.sqlite3 >> checksums.txt

# # Don't backup 'prototyping' namespace
# echo "InfluxDB"
# kubectl -n prototyping cp `get_deployment_pods influxdb prototyping|head`:/var/lib/influxdb influxdb
# tar czf influxdb.tar.xz influxdb
# rm -Rf influxdb
# sha1sum influxdb.tar.xz >> checksums.txt

sha1sum -c checksums.txt
ls -lh .

cd ..
exec python3 upload-backups-gdrive.py "${START_DATE}"
