# Plaza Backups

This directory contains a solution for automated backups for Plaza on a Kubernes cluster. Is not necessary for a normal deployment and it has to be updated when new data storages are added, but it can be useful.

It has two main components, the ServiceAccount, Role and binding files `00-*` to `02-`, which prepare the necessary permissions for the backup operation; and the cronjob `04-cronjob.yaml`, which performs the operation itself.

## Permissions for plaza-backups

The goal of this files is to generate a KUBECONFIG file with only the permissions necessary for plaza-backups. Keep in mind that these are actually pretty extensive and include listing, getting information of, copying from and executing things on pods and deployments. Although no update, creation or delete is allowed, plenty of damage can be done if this kubeconfig is not kept safe.

### Installation

Deploy kubernetes objects:

* `kubectl apply -f 00-backups-sa.yaml`
* `kubectl apply -f 01-backups-role.yaml`
* `kubectl apply -f 02-backups-rolebinding.yaml`

Get service account secret:

* `kubectl get sa plaza-backups-sa -o json| jq .secrets[0].name`

Use `kubeadm` to generate the kubeconfig:

* `kubeadm  alpha kubeconfig user --client-name="plaza-backups" --token="plaza-backups" --token="$(kubectl -n plaza-production get secret -o json $(kubectl -n plaza-production get sa plaza-backups-sa -o json| jq -r .secrets[0].name )|jq -r .data.token |base64 -d)" > plaza-backups.kubeconfig`

If `kubeadm` is installed on the server (`kubespray` does this), this can be done:

* `ssh root@projectplaza.space kubeadm  alpha kubeconfig user --client-name="plaza-backups" --token="plaza-backups" --token="$(kubectl -n plaza-production get secret -o json $(kubectl -n plaza-production get sa plaza-backups-sa -o json| jq -r .secrets[0].name )|jq -r .data.token |base64 -d)" > plaza-backups.kubeconfig`

This `plaza-backups.kubeconfig` file will be uploaded as a secret and used by the cronjob for its operation.

### Uninstall

To remove this run:
* `kubectl delete -f 00-backups-sa.yaml -f 01-backups-role.yaml -f 02-backups-rolebinding.yaml`

## Cronjob

### Preparations

* Generate the `plaza-backups.kubeconfig` (section above).
* Install the necessary python libraries: `pip3 install -U -r requirements.txt`
* Obtain a `credentials.json` file for the Google Drive API (where the backups will be uploaded).
* Run `python3 03-create-secrets.py plaza-backups.kubeconfig credentials.json`.

### Install

Run

* `kubectl apply -f 04-cronjob.yaml`
