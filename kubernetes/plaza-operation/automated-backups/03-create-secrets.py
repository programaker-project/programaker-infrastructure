#!/usr/bin/env python3

from __future__ import print_function

SCOPES = ['https://www.googleapis.com/auth/drive.file']
GDRIVE_SECRET_NAME = "plaza-backups-gdrive-credentials"
KUBECONFIG_SECRET_NAME = "plaza-backups-kubeconfig"
NAMESPACE = "plaza-operation"

import json
import sys
import shlex
import subprocess

from google_auth_oauthlib.flow import InstalledAppFlow


def create_secret(secret_name, namespace, components):
    command = [
        "kubectl", "-n", namespace,
        "create", "secret", "generic", secret_name,
    ]
    for key, value in components.items():
        command.append(shlex.quote("--from-file={}={}".format(key, value)))

    print("+ {}".format(command))
    subprocess.run(command, check=True)

def get_token_from_file(path_to_credentials):
    flow = InstalledAppFlow.from_client_secrets_file(path_to_credentials, SCOPES)
    creds = flow.run_local_server(port=0)
    return creds


def main():
    if len(sys.argv) < 3:
        print("{} <plaza-backups.kuebconfig> <credentials.json> [<token.json>]".format(sys.argv[0]))
        exit(0)

    token_path = "token.json"
    if len(sys.argv) > 3:
        token_path = sys.argv[3]

    credentials = get_token_from_file(sys.argv[2])
    with open(token_path, "wt") as f:
        f.write(credentials.to_json())

    create_secret(GDRIVE_SECRET_NAME, NAMESPACE, {
        "credentials.json": sys.argv[2],
        "token.json": token_path,
    })

    create_secret(KUBECONFIG_SECRET_NAME, NAMESPACE, {
        "kubeconfig.yaml": sys.argv[1],
    })

if __name__ == "__main__":
    main()
