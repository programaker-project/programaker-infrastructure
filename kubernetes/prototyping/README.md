# Prototyping

Sample services to prototype functionality.

This services will not be much maintained, and can be removed at any time. For this reason the setup is not necessarily very solid.

If you're deploying a Plaza cluster for use (and not development) you can safely ignore these.

## Metrics

Metric recording and presentation. In this case using InfluxDB and Grafana.
