#!/bin/sh

set -eu

helm fetch stable/influxdb -d ../../../output/charts/

rm -Rf ../../../output/templates/influxdb/
helm template -f values.yaml --name=influxdb ../../../output/charts/influxdb-1.1.4.tgz \
     --namespace prototyping \
     --output-dir ../../../output/templates/

kubectl apply -n prototyping \
        -f ../../../output/templates/influxdb/templates/config.yaml                          \
        -f ../../../output/templates/influxdb/templates/init-config.yaml                     \
        -f ../../../output/templates/influxdb/templates/deployment.yaml                      \
        -f ../../../output/templates/influxdb/templates/pvc.yaml                             \
        -f ../../../output/templates/influxdb/templates/service.yaml
