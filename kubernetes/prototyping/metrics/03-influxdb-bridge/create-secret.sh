#!/bin/sh

SECRET_NAME=plaza-influxdb-bridge
SECRET_NAMESPACE=prototyping

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>/dev/null 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

read -p "Bridge endpoint: " BRIDGE_ENDPOINT
read -p "InfluxDB user: " INFLUXDB_USER
read -s -p "InfluxDB password (will not be displayed): " INFLUXDB_PASS

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" \
	--from-literal=bridge_endpoint="$BRIDGE_ENDPOINT" \
  --from-literal=influxdb_user="$INFLUXDB_USER" \
  --from-literal=influxdb_pass="$INFLUXDB_PASS" \
