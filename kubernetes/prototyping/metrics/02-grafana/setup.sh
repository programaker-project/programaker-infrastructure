#!/bin/sh

set -eu

helm fetch stable/grafana -d ../../../output/charts/

rm -Rf ../../../output/templates/grafana/
helm template -f values.yaml --name=grafana ../../../output/charts/grafana-3.3.7.tgz \
     --namespace prototyping \
     --output-dir ../../../output/templates/

kubectl apply -n prototyping \
        -f ../../../output/templates/grafana/templates/podsecuritypolicy.yaml \
        -f ../../../output/templates/grafana/templates/tests/test-podsecuritypolicy.yaml \
        -f ../../../output/templates/grafana/templates/secret.yaml \
        -f ../../../output/templates/grafana/templates/configmap.yaml \
        -f ../../../output/templates/grafana/templates/tests/test-configmap.yaml \
        -f ../../../output/templates/grafana/templates/serviceaccount.yaml \
        -f ../../../output/templates/grafana/templates/tests/test-serviceaccount.yaml \
        -f ../../../output/templates/grafana/templates/clusterrole.yaml \
        -f ../../../output/templates/grafana/templates/clusterrolebinding.yaml \
        -f ../../../output/templates/grafana/templates/role.yaml \
        -f ../../../output/templates/grafana/templates/tests/test-role.yaml \
        -f ../../../output/templates/grafana/templates/rolebinding.yaml \
        -f ../../../output/templates/grafana/templates/tests/test-rolebinding.yaml \
        -f ../../../output/templates/grafana/templates/service.yaml \
        -f ../../../output/templates/grafana/templates/tests/test.yaml \
        -f ../../../output/templates/grafana/templates/deployment.yaml
