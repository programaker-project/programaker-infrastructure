#!/bin/sh

set -eu

helm fetch -d ../../output/charts/ stable/nginx-ingress

rm -Rf ../../output/templates/nginx-ingress/
helm template -f values.yaml --name=entrypoint  ../../output/charts/nginx-ingress-1.6.0.tgz --namespace prototyping --output-dir ../../output/templates/
kubectl -n prototyping apply -f ../../output/templates/nginx-ingress/*
