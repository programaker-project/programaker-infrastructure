 #!/usr/bin/env bash

 set -eu

NAMESPACE=${NAMESPACE:-plaza-production}
BACKUP_CONTAINER=${BACKUP_CONTAINER:-plaza-backend-0}
BACKUP_FILE=${BACKUP_FILE:-plaza-$RANDOM-$RANDOM.mnesia.backup}
CHECKSUM_FILE=${CHECKSUM_FILE:-$BACKUP_FILE.sha1}

# Perform backup
set -x  # Echo the next command
kubectl -n "$NAMESPACE" exec "$BACKUP_CONTAINER" ash /app/scripts/backup.sh > "${BACKUP_FILE}" 2>"${CHECKSUM_FILE}"

# Check backup integrity
set +x
cat "${CHECKSUM_FILE}" 
sha1sum "${BACKUP_FILE}"

checksum=`cat ${CHECKSUM_FILE}|cut -d\  -f1`
echo "$checksum ${BACKUP_FILE}" > "${CHECKSUM_FILE}"
sha1sum -c "${CHECKSUM_FILE}"
