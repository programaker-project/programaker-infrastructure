#!/usr/bin/env python3

from __future__ import print_function
import pickle
import os.path
import sys
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaFileUpload


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive.file']

def main():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    # Check arguments now, so the first run can be done without
    #  parameters to setup the credentials.
    if len(sys.argv) < 2:
        print("{} <folder to upload>".format(sys.argv[0]))
        exit(0)

    directory = sys.argv[1]
    backup_directory_name = os.path.basename(directory.rstrip("/\\"))

    print("Preparing backups/{} folder".format(backup_directory_name))

    results = service.files().list(
        q="name = 'backups' and trashed = false",
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    backups_directory_id = None
    if items:
        for item in items:
            print(item['name'])
            if item['name'] == 'backups':
                backups_directory_id = item['id']
                print("  Backups directory found")
                break

    if backups_directory_id is None:
        file_metadata = {
            'name': 'backups',
            'mimeType': 'application/vnd.google-apps.folder'
        }
        file = service.files().create(body=file_metadata,
                                      fields='id').execute()

        backups_directory_id = file.get("id")


    results = service.files().list(
        q="\"{}\" in parents and name = \"{}\"".format(backups_directory_id, backup_directory_name),
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    current_directory_id = None
    if items:
        for item in items:
            print(item['name'])
            if item['name'] == backup_directory_name:
                current_directory_id = item['id']
                print("  Current directory found")
                break

    if current_directory_id is None:
        file_metadata = {
            'name': backup_directory_name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [backups_directory_id],
        }

        file = service.files().create(body=file_metadata,
                                      fields='id').execute()
        current_directory_id = file.get("id")

    for filename in os.listdir(directory):
        print(filename)
        fpath = os.path.join(directory, filename)
        file_metadata = {
            'name': filename,
            'resumable': True,
            'parents': [current_directory_id],
        }

        media = MediaFileUpload(fpath)
        file = service.files().create(body=file_metadata,
                                      media_body=media,
                                      fields='id').execute()

if __name__ == '__main__':
    main()
