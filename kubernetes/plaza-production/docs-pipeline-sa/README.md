# KUBECONFIG for plaza-docs CI

The goal of this is to generate a KUBECONFIG file which only allows to update the plaza-docs deployment.

## Installation

Deploy kubernetes objects:

* `kubectl apply -f 00-docs-pipeline-sa.yaml`
* `kubectl apply -f 01-docs-pipeline-role.yaml`
* `kubectl apply -f 02-docs-pipeline-rolebinding.yaml`

Get service account secret:

* `kubectl -n plaza-production get sa plaza-docs-pipeline-sa -o json| jq .secrets[0].name`

Use `kubeadm` to generate the kubeconfig:

* `kubeadm  alpha kubeconfig user --client-name="plaza-docs-pipeline" --token="plaza-docs-pipeline" --token="$(kubectl -n plaza-production get secret -o json $(kubectl -n plaza-production get sa plaza-docs-pipeline-sa -o json| jq -r .secrets[0].name )|jq -r .data.token |base64 -d)" > plaza-docs-pipeline.kubeconfig`

If `kubeadm` is installed on the server (`kubespray` does this), this can be done:

* `ssh root@projectplaza.space kubeadm  alpha kubeconfig user --client-name="plaza-docs-pipeline" --token="plaza-docs-pipeline" --token="$(kubectl -n plaza-production get secret -o json $(kubectl -n plaza-production get sa plaza-docs-pipeline-sa -o json| jq -r .secrets[0].name )|jq -r .data.token |base64 -d)" > plaza-docs-pipeline.kubeconfig`


## Usage

With this kubeconfig, plaza-docs can be updated programatically from a semi-trusted source:

* `KUBECONFIG=`pwd`/plaza-docs-pipeline.kubeconfig kubectl -n plaza-production get  deployment/plaza-docs docs=plaza-docs:316b24fe930be4fc453b13ef8cdb59a9e0bf1a50`


## Uninstall

To remove this run:
* `kubectl delete -f 00-docs-pipeline-sa.yaml -f 01-docs-pipeline-role.yaml -f 02-docs-pipeline-rolebinding.yaml`
