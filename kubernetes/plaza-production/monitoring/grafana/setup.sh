#!/bin/sh

set -eu

helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

helm install -n plaza-production -f values.yaml plaza-monitoring-grafana grafana/grafana
