#!/bin/sh

set -eu

mkdir config || true

if [ ! -f config/bot-token.txt ];then
    echo -n "Enter bot token (and press Enter): "
    read BOTTOKEN
    echo "$BOTTOKEN" > config/bot-token.txt
else
    echo "Bot token already present (on config/bot-token.txt)"
    BOTTOKEN=`cat config/bot-token.txt`
fi

if [ ! -f config/bot-recipient.txt ];then
    echo -n "Enter chat ID (and press Enter): "
    read CHATID
    echo "$CHATID" > config/bot-recipient.txt
else
    echo "Chat ID already present (on config/bot-recipient.txt)"
    CHATID=`cat config/bot-recipient.txt`
fi

export BOTTOKEN
export CHATID

envsubst < values.tpl > values.yaml
cat dashboard.json | sed 's/^/        /' >> values.yaml
