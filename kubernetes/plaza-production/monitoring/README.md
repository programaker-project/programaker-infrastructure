# How to deploy Plaza monitoring

## (Optional) Update Prometheus operator installer

- Download prometheus-operator bundle: `wget https://raw.githubusercontent.com/coreos/prometheus-operator/master/bundle.yaml -O 00-bundle.yaml`
- Update namespace `sed 's/namespace: *default/namespace: plaza-production/g' -i 00-bundle.yaml`

## Install

- Apply bundle on the `plaza-production` namespace: `kubectl -n plaza-production apply -f 00-bundle.yaml`

## Open Prometheus

- Open a connection to the prometheus service: `kubectl -n plaza-production port-forward svc/prometheus-operated 9090:9090`
- Open a browser on: `localhost:9090`
