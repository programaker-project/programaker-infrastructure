#!/usr/bin/env bash

SECRET_NAME=plaza-google-sheets-bridge-creds
SECRET_NAMESPACE=plaza-production

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>/dev/null 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

if [ -z "$1" ];then
    echo "Usage: $0 <client_credentials.json>"
    exit 1
fi

set -eu

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" --from-file="client_credentials.json=$1"
