#!/bin/sh

SECRET_NAME=plaza-twitter-bridge
SECRET_NAMESPACE=plaza-production

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>/dev/null 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

read -p "Bridge endpoint: " BRIDGE_ENDPOINT
read -r -s -p "Bridge TOKEN (will not be displayed): " BRIDGE_TOKEN
read -p "DB Connection string: " CONNECTION_STRING
read -p "Twitter consumer API token: " TWITTER_TOKEN
read -p "Twitter consumer API token secret: " TWITTER_SECRET

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" \
	--from-literal=bridge_endpoint="$BRIDGE_ENDPOINT" \
	--from-literal=bridge_token="$BRIDGE_TOKEN" \
	--from-literal=connection_string="$CONNECTION_STRING" \
	--from-literal=twitter_token="$TWITTER_TOKEN" \
	--from-literal=twitter_secret="$TWITTER_SECRET"
