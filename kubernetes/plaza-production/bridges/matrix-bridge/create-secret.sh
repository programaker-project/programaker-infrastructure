#!/bin/sh

SECRET_NAME=plaza-matrix-bridge
SECRET_NAMESPACE=plaza-production

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>/dev/null 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

read -r -p "Bridge endpoint: " BRIDGE_ENDPOINT
read -r -s -p "Bridge TOKEN (will not be displayed): " BRIDGE_TOKEN
read -p "DB Connection string: " CONNECTION_STRING
read -r -p "Matrix instance: " MATRIX_INSTANCE
read -r -p "Matrix bot name (full address for registration popup): " MATRIX_BOT_ADDRESS
read -r -p "Matrix bot name (for bot login): " MATRIX_BOT_NAME
read -r -s -p "Matrix bot password (will not be displayed): " MATRIX_BOT_PASSWORD

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" \
	--from-literal=bridge_endpoint="$BRIDGE_ENDPOINT" \
	--from-literal=bridge_token="$BRIDGE_TOKEN" \
	--from-literal=connection_string="$CONNECTION_STRING" \
	--from-literal=matrix_instance="$MATRIX_INSTANCE" \
	--from-literal=matrix_bot_address="$MATRIX_BOT_ADDRESS" \
	--from-literal=matrix_bot_name="$MATRIX_BOT_NAME" \
	--from-literal=matrix_bot_password="$MATRIX_BOT_PASSWORD"
