# plaza-infrastructure

Plaza infrastructure tools.

On ubuntu 16.04 install helm

```sh
sudo snap install helm --classic
git clone https://gitlab.com/plaza-project/plaza-infrastructure
cd kubernetes
sh configure.sh
```
Once you have this, if you do 

```sh
kubectl get pods -n plaza-production
```
you would see the ingress

```sh
cd rook-ceph
```

Now by order we have to launch the yaml files

``` sh
kubectl create -f 00-common.yaml
kubectl create -f 01-operator.yaml
kubectl create -f 02-cluster.yaml
kubectl create -f 03-storageclass.yaml
```



