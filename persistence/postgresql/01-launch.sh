#!/usr/bin/env bash

set -eu

cd "$(dirname $0)"

docker run -p 5432:5432 -d --name=persistence-postgres  \
       -e POSTGRES_PASSWORD_FILE=/run/secrets/postgres-passwd \
       -e CONFIG_FILE=/etc/postgres/config/pg_hba.conf \
       -v persistence_postgres_data:/var/lib/postgresql/data/ \
       -v `pwd`/config/secrets:/run/secrets \
       -v `pwd`/config/initialization:/docker-entrypoint-initdb.d \
       postgres:12
