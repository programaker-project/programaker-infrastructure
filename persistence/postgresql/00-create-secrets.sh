#!/usr/bin/env bash

set -eu

cd "$(dirname $0)"

PASSWD_FILE="config/secrets/postgres-passwd"

if [ ! -f "${PASSWD_FILE}" ]; then
    echo "Creating ${PASSWD_FILE}"
    openssl rand -base64 32|tr -d '/+=' > "${PASSWD_FILE}"
else
    echo "Skipping ${PASSWD_FILE} (already exists)"
fi
