#!/usr/bin/env bash

set -eu

cd "$(dirname $0)"

docker run --restart=unless-stopped --name=persistence-nfs -d \
       -p 111:111 -p 111:111/udp \
       -p 2049:2049 -p 2049:2049/udp \
       -p 32765:32765 \
       -p 32767:32767 \
       -v `pwd`/config/hosts.allow:/etc/hosts.allow:ro \
       -v `pwd`/config/hosts.deny:/etc/hosts.deny:ro \
       -v `pwd`/config/exports.txt:/etc/exports:ro \
       -v /shared:/shared \
       --cap-add SYS_ADMIN \
       --cap-add SYS_MODULE \
       erichough/nfs-server
