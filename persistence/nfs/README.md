# Installation

1. Move this directory to the intended NFS server.
1. Copy `config/hosts.allow.orig` to `config/hosts.allow` and edit in the necessary values (the ones between `<` and `>`).
1. Copy `config/exports.txt.orig` to `config/exports.txt` and edit in the necessary values (the ones between `<` and `>`).
1. Run `bash 01-launch.sh`.
