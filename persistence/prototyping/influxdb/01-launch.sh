#!/usr/bin/env bash

set -eu

cd "$(dirname $0)"

docker run -p 8086:8086 -d --name=proto-influxdb  \
       -e INFLUXDB_ADMIN_USER=admin \
       -v `pwd`/config/influxdb.conf:/etc/influxdb/influxdb.conf:ro \
       -v proto_persistence_influxdb:/var/lib/influxdb/ \
       influxdb:1.7
