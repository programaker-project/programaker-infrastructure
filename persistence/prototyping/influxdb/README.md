# Installation

**Note that this piece of infrastructure is being used for prototyping (in the context of Plaza/PrograMaker). It is not a good idea to rely on it as part of the infrastructure yet.**

**Note that this server does not auto-configure user authentication, that has to be done manually after is launched.**

1. Move this directory to the intended Influx server.
1. Run `bash 01-launch.sh**.
